# Changelog

### [v1.2.0] - 2024-05-12

* Display name for groups support added
* Adjusted namespaces to be consistent
* XMPP module cleanup and additional testing
* Improved build logging
* Dependency updates
* Updated Docker image to 3.11

### [v1.0.0] - 2022-04-27

* Added Poetry build system
* Refactored project, breaking apart components
* Added sync command for continually running syncs at interval
* Updated two stage Docker build with Poetry
* Added support for Evolution E164 vCards ([Lily](https://gitlab.com/0chroma))
* Added support for default categories ([Lily](https://gitlab.com/0chroma))
* Updated to Python 3.10
* Added configuration file validation

### [v0.0.5] - 2021-12-23

* Fixed Subscription/Authorization Issue that prevented syncing in ejabberd XMPP Servers
* Added support for loading vCards from file instead of WebDav

### [v0.0.4] - 2021-12-14

* Added dev/CI requirements
* Added better error reporting if no vCards found
* Patch for xmpppy issue 52

### [v0.0.3] - 2021-05-26

* Added LICENSE
* Added Screenshots
* Updated dependencies

### [v0.0.2] (2021-05-23)

* Updated Dockerfile
* Updated README/documentation

### [v0.0.1] (2021-05-17)

* Initial Release
* Implemented full WebDAV to XMPP Sync
* See [Moving my phone numbers from Google Hangouts/Voice to an SIP/XMPP Service](https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/) for basic concept and motivations
