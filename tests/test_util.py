# Written by Sumit Khanna
# License: AGPLv3
# https://battlepenguin.com
import unittest
from davxmppsync.util import normalize_phone_numbers, extract_clan


class XMPPTests(unittest.TestCase):

    def test_normalize_phone_numbers(self):
        self.assertEqual(normalize_phone_numbers('+1 (555) 423-1818'), '+15554231818')

    def test_extract_clan(self):
        self.assertEqual(extract_clan("[CrAsH] Ricky, Moe, Jason"), "[CrAsH] ")
        self.assertEqual(extract_clan("[Some Group] Ellie, Jenny Cate, Ava"), "[Some Group] ")
