# Written by Sumit Khanna
# https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/
# License: AGPLv3
import unittest
from davxmppsync.validator import validate_config
import yaml


class ValidatorTests(unittest.TestCase):

    carddav_config = """carddav:
  url: http://localhost/
  username: myuser
  password: s0m3pa33w0rd
xmpp:
  jid: myuser@xmpp.example.com
  password: some_xmpp_pAssw0rd
  debug: False
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: sms.example.net
    """

    local_vcard_config = """vcards:
  file: /tmp/sample-cards.vcards
xmpp:
  jid: myuser@xmpp.example.com
  password: some_xmpp_pAssw0rd
  debug: False
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: sms.example.net
    """

    invalid_carddav_and_vcard = """vcards:
  file: /tmp/sample-cards.vcards
carddav:
  url: http://localhost/
  username: myuser
  password: s0m3pa33w0rd
xmpp:
  jid: myuser@xmpp.example.com
  password: some_xmpp_pAssw0rd
  debug: False
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: sms.example.net
    """

    invalid_xmpp_debug = """vcards:
  file: /tmp/sample-cards.vcards
carddav:
  url: http://localhost/
  username: myuser
  password: s0m3pa33w0rd
xmpp:
  jid: myuser@xmpp.example.com
  password: some_xmpp_pAssw0rd
  debug: 12
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: sms.example.net
    """

    invalid_carddav_url = """carddav:
  url: abcd
  username: myuser
  password: s0m3pa33w0rd
xmpp:
  jid: myuser@xmpp.example.com
  password: some_xmpp_pAssw0rd
  debug: False
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: sms.example.net
    """

    invalid_group_sync = """carddav:
  url: http://localhost/
  username: myuser
  password: s0m3pa33w0rd
xmpp:
  jid: myuser@xmpp.example.com
  password: some_xmpp_pAssw0rd
  debug: False
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: sms.example.net
  include_group_chats: "true"
    """

    def assertInLog(self, line, logs):
        self.assertTrue(any(line in li for li in logs))

    def test_valid_carddav_config(self):
        config = yaml.safe_load(ValidatorTests.carddav_config)
        self.assertTrue(validate_config(config))

    def test_valid_vcard_config(self):
        config = yaml.safe_load(ValidatorTests.local_vcard_config)
        self.assertTrue(validate_config(config))

    def test_invalid_carddav_and_vcard(self):
        config = yaml.safe_load(ValidatorTests.invalid_carddav_and_vcard)
        with self.assertLogs('dav-xmpp-sync') as lg:
            self.assertFalse(validate_config(config))
            self.assertInLog('You must specify carddav or vcards, not both', lg.output)

    def test_invalid_xmpp_debug(self):
        config = yaml.safe_load(ValidatorTests.invalid_xmpp_debug)
        with self.assertLogs('dav-xmpp-sync') as lg:
            self.assertFalse(validate_config(config))
            self.assertInLog('xmpp:debug must be True or False', lg.output)

    def test_invalid_carddav_url(self):
        config = yaml.safe_load(ValidatorTests.invalid_carddav_url)
        with self.assertLogs('dav-xmpp-sync') as lg:
            self.assertFalse(validate_config(config))
            self.assertInLog('Invalid carddav URL abcd', lg.output)

    def test_invalid_sync_group_chats_setting(self):
        config = yaml.safe_load(ValidatorTests.invalid_group_sync)
        with self.assertLogs('dav-xmpp-sync') as lg:
            self.assertFalse(validate_config(config))
            self.assertInLog('sync:include_group_chats must be True or False', lg.output)
