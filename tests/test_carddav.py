# Written by Sumit Khanna
# https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/
# License: AGPLv3
import unittest
import pytest
from davxmppsync.carddav import parsecard, filter_cards_with_numbers, \
    read_caldav, read_vcards, split_all_vcards, DEFAULT_CATEGORY
from davxmppsync.util import normalize_phone_numbers
import responses
import tempfile
import os
import uuid


class CardDavTests(unittest.TestCase):

    one_number_card = """VERSION:3.0
UID:dbbc47b6-c2da-481d-9f14-8cb2d821217d
CATEGORIES: United States
EMAIL;TYPE=INTERNET,HOME:mike@example.com
EMAIL;TYPE=INTERNET:mike@example.net
FN:Mike FooMan
N:FooMan;Mike;;;
TEL;TYPE=CELL:+1 423-555-6668
item1.X-ABLABEL:PROFILE
X-RADICALE-NAME:4E8637F4-3CE1E309-C15998AF.vcf
    """

    no_number_card = """VERSION:3.0
UID:09e43f1c-46c1-47b4-80f9-11d58aa4a7bc
ADR;TYPE=HOME:;;9999 S Odessa St;Nowhere;NY;00000;
CATEGORIES: United States,Seattle
EMAIL;TYPE=INTERNET:x@gmail.com
FN:Travis TheDude
N:TheDude;Travis;;;
X-RADICALE-NAME:4E6FD17D-54BF7751-A6A75C1F.vcf
"""

    multi_number_card = """VERSION:3.0
UID:ccdc36a0-ce46-4d79-9550-269ff66fdc4c
CATEGORIES: United States
EMAIL;TYPE=INTERNET,HOME:x@msn.com
FN:Daniel TheBee
N:TheBee;Daniel;;;
TEL;TYPE=CELL:+1 423-555-1111
TEL;TYPE=HOME:+1 706-555-2222
TEL;TYPE=WORK:+1 314-555-3333
X-RADICALE-NAME:614D1B75-713A2BB9-4714BE83.vcf
"""

    xevolution_e164_card = """VERSION:3.0
PRODID:-//EteSync//com.etesync.syncadapter/0.20.4 ez-vcard/0.10.2
UID:4a28cd46-3be6-4819-92b6-1d4a9e364d4e
FN:First Last
N:Last;First;;;
REV:20180429T234922Z
TEL;TYPE=cell;X-EVOLUTION-E164=6664206666,+1:+16664206666
X-EVOLUTION-FILE-AS:Last\\, First
X-EVOLUTION-WEBDAV-ETAG:4982caa7e8df5cfd450a1df21229ac0852b7b1eba78aaa3e3c7
 3c5ac5a041c3f
"""

    def setUp(self):

        card_string_lst = [CardDavTests.one_number_card,
                           CardDavTests.no_number_card,
                           CardDavTests.multi_number_card,
                           CardDavTests.xevolution_e164_card]

        self.default_test_category = DEFAULT_CATEGORY

        self.random_test_category = str(uuid.uuid4())

        self.all_cards = [parsecard(c, self.default_test_category) for c in card_string_lst]

        self.vcard_tmpfile = tempfile.mkstemp(suffix='.vcf')
        vcards_string = ''.join([f'BEGIN:VCARD\n{f}END:VCARD\n' for f in card_string_lst])
        with open(self.vcard_tmpfile[1], 'w') as fd:
            fd.write(vcards_string)

    def tearDown(self):
        os.unlink(self.vcard_tmpfile[1])

    def test_bad_config_both_vcards_carddav_sections(self):
        with self.assertLogs('dav-xmpp-sync', level='ERROR') as cm:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                read_vcards({'carddav': {}, 'vcards': {}})
            self.assertEqual(pytest_wrapped_e.type, SystemExit)
            self.assertEqual(pytest_wrapped_e.value.code, 3)
            self.assertEqual(cm.output, ['ERROR:dav-xmpp-sync:You may specify either vcards or carddav, not both.'])

    def test_bad_config_no_contact_retrieval(self):
        with self.assertLogs('dav-xmpp-sync', level='ERROR') as cm:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                read_vcards({'xmpp': {}, 'sync': {}})
            self.assertEqual(pytest_wrapped_e.type, SystemExit)
            self.assertEqual(pytest_wrapped_e.value.code, 3)
            self.assertEqual(cm.output, ['ERROR:dav-xmpp-sync:No contact retrieval defined in config. You must have a vcards or carddav section.'])

    def test_vcard_file_load(self):
        cards = read_vcards({'vcards': {'file': self.vcard_tmpfile[1]}})
        self.assertEqual(len(cards), 4)

    def test_vcard_file_load_default_categories(self):
        cards = read_vcards({'vcards': {'file': self.vcard_tmpfile[1], 'default_category': self.random_test_category}})
        self.assertEqual(len(cards), 4)
        self.assertEqual(cards[0]['tags'], ['United States'])
        self.assertEqual(cards[3]['tags'], [self.random_test_category])

    @responses.activate
    def test_vcard_webdav_load_default_categories(self):
        card_body = ''.join([f'BEGIN:VCARD\n{f}END:VCARD\n' for f in [CardDavTests.one_number_card,
                                                                      CardDavTests.xevolution_e164_card]])

        url = 'http://example.com/Contacts.vcf'
        responses.add(responses.GET, url, body=card_body, status=200)
        cards = read_vcards({'carddav': {'url': url, 'username': '', 'password': '', 'default_category': self.random_test_category}})
        self.assertEqual(cards[0]['tags'], ['United States'])
        self.assertEqual(cards[1]['tags'], [self.random_test_category])

    @responses.activate
    def test_invalid_carddav(self):
        url = 'http://example.com/Contacts.vcf'
        responses.add(responses.GET, url, body='404 Not Found', status=404)

        with self.assertLogs('dav-xmpp-sync', level='INFO') as cm:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                split_all_vcards(read_caldav(url, 'user', 'pass'))
            self.assertEqual(pytest_wrapped_e.type, SystemExit)
            self.assertEqual(pytest_wrapped_e.value.code, 2)
            self.assertEqual(cm.output, ['INFO:dav-xmpp-sync:Requesting vCards From http://example.com/Contacts.vcf',
                                         'ERROR:dav-xmpp-sync:No VCARDs found'])

    @responses.activate
    def test_valid_carddav(self):
        card_body = ''.join([f'BEGIN:VCARD\n{f}END:VCARD\n' for f in [CardDavTests.one_number_card,
                                                                      CardDavTests.xevolution_e164_card]])

        url = 'http://example.com/Contacts.vcf'
        responses.add(responses.GET, url, body=card_body, status=200)
        raw_cards = read_caldav(url, 'user', 'pass')
        cards = split_all_vcards(raw_cards)
        self.assertEqual(len(cards), 2)

    def test_one_num_card(self):
        one_num = parsecard(CardDavTests.one_number_card, self.default_test_category)
        self.assertEqual(one_num, {'uid':
                                   'dbbc47b6-c2da-481d-9f14-8cb2d821217d',
                                   'numbers': {'cell': ['+14235556668']}, 'full_name': 'Mike FooMan', 'tags': ['United States']})

    def test_xevolution_e164_card(self):
        e164 = parsecard(CardDavTests.xevolution_e164_card, self.default_test_category)
        self.assertEqual(e164, {'uid':
                                '4a28cd46-3be6-4819-92b6-1d4a9e364d4e',
                                'numbers': {'cell': ['+16664206666']}, 'full_name': 'First Last', 'tags': [self.default_test_category]})

    def test_xevolution_e164_card_default_category(self):
        e164 = parsecard(CardDavTests.xevolution_e164_card, self.random_test_category)
        self.assertEqual(e164, {'uid':
                                '4a28cd46-3be6-4819-92b6-1d4a9e364d4e',
                                'numbers': {'cell': ['+16664206666']}, 'full_name': 'First Last', 'tags': [self.random_test_category]})

    def test_no_num_card(self):
        no_num = parsecard(CardDavTests.no_number_card, self.default_test_category)
        self.assertEqual(no_num, {'uid':
                                  '09e43f1c-46c1-47b4-80f9-11d58aa4a7bc',
                                  'numbers': {}, 'full_name': 'Travis TheDude', 'tags': ['United States', 'Seattle']})

    def test_multi_num_card(self):
        multi_num = parsecard(CardDavTests.multi_number_card, self.default_test_category)
        self.assertEqual(multi_num, {'uid':
                                     'ccdc36a0-ce46-4d79-9550-269ff66fdc4c',
                                     'numbers': {'cell': ['+14235551111'], 'home': ['+17065552222'], 'work': ['+13145553333']}, 'full_name': 'Daniel TheBee', 'tags': ['United States']})

    def test_normalize_phone_numbers(self):
        self.assertEqual(normalize_phone_numbers('+1 615 555-1248'), '+16155551248')
        self.assertEqual(normalize_phone_numbers('+64 027 6666'), '+640276666')
        self.assertEqual(normalize_phone_numbers('+4 123-45-67 89'), '+4123456789')
        self.assertEqual(normalize_phone_numbers('+1(513)5551234'), '+15135551234')

    def test_filter_cards_with_numbers(self):
        r = filter_cards_with_numbers(self.all_cards, 'cell')
        self.assertEqual(len(r), 3)
