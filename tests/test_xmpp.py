# Written by Sumit Khanna
# License: AGPLv3
# https://battlepenguin.com
import unittest
from unittest.mock import patch, call

import xmpp.roster

from davxmppsync.xmpp import new_groups, UpdateXMPP, add_new_cards, ensure_dual_subscriptions, update_display_names, update_group_chat_names


class XMPPTests(unittest.TestCase):

    def setUp(self):
        self.managed_groups = ['Family', 'Friends', 'Cincinnati']

    def test_add_no_groups(self):
        dav_groups = []
        xmpp_groups = []
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, [])

    def test_add_some_managed_groups(self):
        dav_groups = ['Family', 'Cincinnati']
        xmpp_groups = []
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, dav_groups)

    def test_add_some_managed_some_unmanaged_groups(self):
        dav_groups = ['Family', 'Cincinnati']
        xmpp_groups = ['Family', 'Recent']
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, ['Family', 'Cincinnati', 'Recent'])

    def test_remove_some_managed_groups(self):
        dav_groups = ['Cincinnati']
        xmpp_groups = ['Family', 'Cincinnati', 'Friends']
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, ['Cincinnati'])

    def test_remove_some_managed_groups_some_unmanaged(self):
        dav_groups = ['Cincinnati']
        xmpp_groups = ['Family', 'Cincinnati', 'InternetFriends']
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, ['Cincinnati', 'InternetFriends'])

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {'+15551237777@cheogram.com': {}})
    @patch.object(xmpp.roster.Roster, 'Subscribe')
    @patch.object(xmpp.roster.Roster, 'Authorize')
    def test_add_new_cards(self, mock_Subscribe, mock_Authorize):
        # TODO: add_new_cards should probably ignore unmanaged groups
        managed_groups = []
        cards_by_number = {
            '+15551237777': {'name': 'Jolly Good'},
            '+15554568888': {'name': 'Humble Bad'},
            '+15554569999': {'name': 'Country Road'}
        }
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, False, cards_by_number, managed_groups)
        add_new_cards(xmpp_update)
        mock_Subscribe.assert_has_calls([call('+15554568888@cheogram.com'), call('+15554569999@cheogram.com')])
        mock_Authorize.assert_has_calls([call('+15554568888@cheogram.com'), call('+15554569999@cheogram.com')])

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {
        '+15552225555@cheogram.com': {'subscription': 'both'},
        '+15551114444@cheogram.com': {'subscription': 'from'},
        '+15553336666@cheogram.com': {'subscription': 'to'},
    })
    @patch.object(xmpp.roster.Roster, 'Subscribe')
    @patch.object(xmpp.roster.Roster, 'Authorize')
    def test_ensure_dual_subscriptions(self, mock_Subscribe, mock_Authorize):
        managed_groups = []
        cards_by_number = {}
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, False, cards_by_number, managed_groups)
        ensure_dual_subscriptions(xmpp_update)
        mock_Subscribe.assert_has_calls([call('+15551114444@cheogram.com'), call('+15553336666@cheogram.com')])
        mock_Authorize.assert_has_calls([call('+15551114444@cheogram.com'), call('+15553336666@cheogram.com')])

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {
        '+15552225555@cheogram.com': {'subscription': 'both'},
        '+15551114444@cheogram.com': {'subscription': 'from'},
        '+15553336666@cheogram.com': {'subscription': 'to'},
    })
    @patch.object(xmpp.roster.Roster, 'Subscribe')
    @patch.object(xmpp.roster.Roster, 'Authorize')
    def test_ensure_dual_subscriptions_dryrun(self, mock_Subscribe, mock_Authorize):
        managed_groups = []
        cards_by_number = {}
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, True, cards_by_number, managed_groups)
        ensure_dual_subscriptions(xmpp_update)
        mock_Subscribe.assert_not_called()
        mock_Authorize.assert_not_called()

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {
        '+15551237777@cheogram.com': {'name': 'Jolly Bad', 'groups': ['Chicago']},
    })
    @patch.object(xmpp.roster.Roster, 'setItem')
    def test_update_display_names(self, mock_SetItem):
        managed_groups = ['Chicago', 'Wellington']
        cards_by_number = {'+15551237777': {'name': 'Jolly Good', 'groups': ['Chicago', 'Denmark']}}
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, False, cards_by_number, managed_groups)
        update_display_names(xmpp_update)
        mock_SetItem.assert_has_calls([call('+15551237777@cheogram.com', name='Jolly Good', groups=['Chicago', 'Denmark'])])

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {
        '+15551237777@cheogram.com': {'name': 'Jolly Bad', 'groups': ['Chicago']},
    })
    @patch.object(xmpp.roster.Roster, 'setItem')
    def test_update_display_names_dry_run(self, mock_SetItem):
        managed_groups = ['Chicago', 'Wellington']
        cards_by_number = {'+15551237777': {'name': 'Jolly Good', 'groups': ['Chicago', 'Denmark']}}
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, True, cards_by_number, managed_groups)
        update_display_names(xmpp_update)
        mock_SetItem.assert_not_called()

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {
        '+645875555,+641111212,+6488999@cheogram.com': {'name': '+645875555,+641111212,+6488999'},
        '+15551112222,+15552223333@cheogram.com': {'name': None},
        '+15558888888,+15552223333@cheogram.com': {'name': None},
        '+15551112222,+641111212@cheogram.com': {'name': "[My Clan] Old Name,Foo Bar"},
    })
    @patch.object(xmpp.roster.Roster, 'setItem')
    def test_update_group_chat_names(self, mock_SetItem):
        managed_groups = []
        cards_by_number = {
            '+645875555': {'name': 'Haily Hay'},
            '+641111212': {'name': 'Baily Bee'},
            '+6488999': {'name': 'Candy See'},
            '+15551112222': {'name': 'Delta Dory'},
            '+15552223333': {'name': 'Echo Echelon'},
        }
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, False, cards_by_number, managed_groups)
        update_group_chat_names(xmpp_update)
        mock_SetItem.assert_has_calls([
            call('+645875555,+641111212,+6488999@cheogram.com', name='Haily Hay, Baily Bee, Candy See'),
            call('+15551112222,+15552223333@cheogram.com', name='Delta Dory, Echo Echelon'),
            call('+15558888888,+15552223333@cheogram.com', name='+15558888888, Echo Echelon'),
            call('+15551112222,+641111212@cheogram.com', name='[My Clan] Delta Dory, Baily Bee')
        ])

    @patch.object(xmpp.roster.Roster, 'getRawRoster', lambda x: {
        '+15551112222,+15552223333@cheogram.com': {'name': None},
    })
    @patch.object(xmpp.roster.Roster, 'setItem')
    def test_update_group_chat_names_dry_run(self, mock_SetItem):
        managed_groups = []
        cards_by_number = {
            '+15551112222': {'name': 'Delta Dory'},
            '+15552223333': {'name': 'Echo Echelon'},
        }
        roster = xmpp.roster.Roster()
        xmpp_update = UpdateXMPP('cheogram.com', roster, True, cards_by_number, managed_groups)
        update_group_chat_names(xmpp_update)
        mock_SetItem.assert_not_called()
