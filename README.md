# dav-xmpp-sync

<a href="https://liberapay.com/djsumdog/donate"><img src="https://img.shields.io/liberapay/patrons/djsumdog.svg?logo=liberapay"></a>

<a href='https://ko-fi.com/V7V425JVS' target='_blank'><img height='24' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=3' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

dav-xmpp-sync is a script that can synchronize contact name from a CardDav server to an XMPP server that's used as an SMS gateway. I've tested it using [Radicale](https://radicale.org) as my CardDav server and [jmp.chat](https://jmp.chat/) as my SMS to XMPP gateway. Your mileage may vary.

See also: [Moving my phone numbers from Google Hangouts/Voice to an SIP/XMPP Service](https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/)

## Screenshots

![Example output of contact synchronization](https://battlepenguin.com/images/tech/google-hangouts-to-xmpp/xmpp-sync-screenshot-1.png)

![Example output of XMPP Roster List](https://battlepenguin.com/images/tech/google-hangouts-to-xmpp/xmpp-sync-screenshot-2.png)

## Installation

You can clone the source and use poetry to setup the virtual environment

```
pip install --user poetry
git clone https://gitlab.com/djsumdog/dav-xmpp-sync.git
cd dav-xmpp-sync
poetry install
poetry run python3 -m davxmppsync --help
```

or use the Docker container and set your configuration via environment variables

```
docker run --rm -it \
    -e CARDDAV_URL=https://example.com/djsumdog/Contacts.vcf/ \
    -e CARDDAV_USERNAME=djsumdog \
    -e CARDDAV_PASSWORD=s0mepa33w0rd \
    -e DEFAULT_CATEGORY=ManagedContacts \
    -e XMPP_JID=myphone@xmpp.example.net \
    -e XMPP_PASSWORD=p1rates \
    -e SYNC_NUMBER_TYPE=cell \
    -e SYNC_INCLUDE_GROUP_CHATS=True \
    -e SYNC_NUMBER_PREFIX=+1 battlepenguin/dav-xmpp-sync --verbose --dry-run sync
```

or mount a configuration file into a container

```
docker run --rm -it -v $PWD/config.yaml:/app/config.yaml battlepenguin/dav-xmpp-sync --verbose --dry-run sync
```

## Configuration

You should create a `config.yaml` that looks like the following:

```yaml
carddav:
  url: https://example.com/your_carddav_account/Contacts.vcf/
  username: your_username
  password: y0ur3pa33w0rd
  default_category: Uncategorized # optional
xmpp:
  jid: ohio@xmpp.example.net
  password: yO4r0the4Pa33word
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: cheogram.com
  include_group_chats: True
```

You can also use a file containing vCards instead of a CardDav server

```yaml
vcards:
  file: mycontacts.vcf
  default_category: Uncategorized # optional
xmpp:
  jid: ohio@xmpp.example.net
  password: yO4r0the4Pa33word
sync:
  number_type: cell
  number_prefix: "+1"
  gateway_domain: cheogram.com
  include_group_chats: True
```

vCard files can also be used via Docker by setting the `CONTACT_SOURCE` variable to `file` and then setting the `VCARD_FILE` variable to where you intend to mount your file.

```
docker run --rm -it \
    -v /home/me/contacts.vcf:/import/contacts.vcf \
    -e CONTACT_SOURCE=file \
    -e VCARD_FILE=/import/contacts.vcf \
    -e XMPP_JID=myphone@xmpp.example.net \
    -e XMPP_PASSWORD=p1rates \
    -e SYNC_NUMBER_TYPE=cell \
    -e SYNC_INCLUDE_GROUP_CHATS=True \
    -e SYNC_NUMBER_PREFIX=+1 battlepenguin/dav-xmpp-sync --verbose --dry-run sync
```

| YAML Parameter | Docker Environment Variable | Description | Required |
|-|-|-|-|
| carddav/url | CARDDAV_URL| Full URL to CardDav (contacts) endpoint | yes (for carddav) |
| carddav/username | CARDDAV_USERNAME| BasicAuth username | yes (for carddav) |
| carddav/password | CARDDAV_PASSWORD| BasicAuth password | yes (for carddav) |
| carddav/category | DEFAULT_CATEGORY | Default Category for vCards without Any | no (defaults to Uncategorized) |
| vcards/file | VCARD_FILE | vCard file to use | yes (for vcard file) |
| vcards/category | DEFAULT_CATEGORY | Default Category for vCards without Any | no (defaults to Uncategorized) |
| - | CONTACT_SOURCE | Select between carddav and vcards (docker only, defaults to carddav) | no |
| xmpp/jid | XMPP_JID | JID (normally username@server) for XMPP | yes |
| xmpp/password | XMPP_PASSWORD | XMPP Password | yes |
| sync/number_type | SYNC_NUMBER_TYPE | Filter on certain number type (e.g. home, cell, work). | no |
| sync/number_prefix | SYNC_NUMBER_PREFIX | Filter by country code (All numbers must have +code. See assumptions) | no |
| sync/gateway_domain | SYNC_GATEWAY_DOMAIN | XMPP to SMS gateway server domain | yes |
| sync/include_group_chats | SYNC_INCLUDE_GROUP_CHATS | Update SMS group chat numbers to a list of names (defalt: False) | no |

## Usage

Options come before command. See `--help`. **You will need to run `sync` at least two times,** the first to authorize and subscribe each phone number to the gateway, and a second time to update display names. This is due to the delay between the auth/subscription calls and when the phone number is added to your roster.

### Groups

Jmp.Chat places SMS text message groups into a single XMPP chat with the JID being a coma separated list of phone numbers. If you enable `include_group_chats` in the `sync` options, the display names for group chats will be updated for phone numbers that are in the contact list. If you want to use this option with groups that have custom names, simply place those names in brackets, so they don't get reset on updates.

For example, if a group display name is set to "Sue, Ana, John" after a sync. You can rename it to "[Boardgames] Sue, Ana, John" and "[Boardgames]" will be preserved between syncs. Any changes to the display name not in brackets will be overwritten.  

**Known Issue**: Group chats must be on the XMPP roster. Group chats you start will be on the roster. Group chats started by others will need to be added.

### Options

| Option | Description | Default |
|-|-|-|
| --config | YAML configuration file | config.yaml |
| --dry-run | Don't Preform any XMPP Updates | (not set) |
| --verbose/--silent | Additional output for debugging | (silent) |

### Commands

* `categories` - List the number of cards tagged with each category
* `contacts` - List CardDav contacts in a table with display name, phone numbers and category tags
* `roster` - Lists names, numbers, subscription status and category tags on the XMPP server
* `sync` - Synchronize contacts from CardDav to XMPP (use --dry-run to test first)
* `service` - Runs as a foreground service, syncing at configured interval


## Assumptions

* If you use the `number_prefix` filter, all your phone numbers have a country code prefix (e.g. +1, +64, ...)
* Your CardDav server is protected by basic auth
* Your CardDav categories are stored as attributes on each individual vCard
* Each vCard in your CardDav server has an `FN` and `UID` attribute
* The phone numbers to be synced are unique and not duplicated across vCards
* Deletes are not implemented; must be handled manually

## License

GNU Affero General Public License v3
