FROM python:3.11 AS builder

RUN mkdir -p /app

COPY poetry.lock /app
COPY pyproject.toml /app
ADD davxmppsync /app/davxmppsync

WORKDIR /app

RUN pip install poetry
RUN poetry install
RUN poetry build

FROM python:3.11

COPY --from=builder /app/dist/*whl /tmp
RUN pip install /tmp/dav_xmpp_sync*whl
RUN rm -f /tmp/*whl

ENV CARDDAV_URL https://example.com/bob/Contacts.vcf/
ENV CARDDAV_USERNAME bob
ENV CARDDAV_PASSWORD bobspassword
ENV CONTACT_SOURCE carddav
ENV VCARD_FILE /import/contacts.vcf
ENV DEFAULT_CATEGORY Uncategorized
ENV XMPP_JID bob@example.com
ENV XMPP_PASSWORD bobspassword
ENV SYNC_NUMBER_TYPE cell
ENV SYNC_NUMBER_PREFIX +1
ENV SYNC_GATEWAY_DOMAIN cheogram.com
ENV SYNC_INCLUDE_GROUP_CHATS False

RUN adduser --system --shell /bin/sh --home /app app
COPY docker-run.sh /app/docker-run
RUN chmod 755 /app /app/docker-run

USER app

ENTRYPOINT ["/app/docker-run"]


