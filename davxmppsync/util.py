#!/usr/bin/env python
# Written by Sumit Khanna
# License: AGPLv3
# https://battlepenguin.com
import re


def normalize_phone_numbers(number):
    return re.sub(r'[\s\(\)-]', '', number)


def extract_clan(input_string):
    match = re.search(r'\[(.*?)\]', input_string)
    if match:
        return f"[{match.group(1)}] "
    return ""
