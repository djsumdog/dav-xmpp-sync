from typing import Dict, List

from xmpp.roster import Roster

from .__init__ import log
from dataclasses import dataclass
import xmpp

from .util import extract_clan


def new_groups(dav_groups, xmpp_groups, managed_groups):
    """Resolves CardDav groups with those that are managed.
       We want to managed adding and deleting from categories defined in CardDav,
       but not remove people from XMPP groups, such as if someone creates a group
       such as "Recent" or "Favourites," to be managed outside their contacts.
    """
    unmanaged = [x for x in xmpp_groups if x not in managed_groups]
    return dav_groups + unmanaged


def update_roster(dry_run, xmpp_conn, cards_by_number, gateway_domain, managed_groups, include_group_chats):
    roster_obj = xmpp_conn.getRoster()
    update_xmpp = UpdateXMPP(gateway_domain, roster_obj, dry_run, cards_by_number, managed_groups)

    add_new_cards(update_xmpp)
    ensure_dual_subscriptions(update_xmpp)
    update_display_names(update_xmpp)
    if include_group_chats:
        update_group_chat_names(update_xmpp)


@dataclass
class UpdateXMPP:
    """Roster Update data structure"""
    gateway_domain: str
    roster_obj: Roster
    dry_run: bool
    cards_by_number: Dict
    managed_groups: List[str]

    def at_gw_domain(self):
        return f'@{self.gateway_domain}'

    def raw_roster(self):
        return self.roster_obj.getRawRoster()

    def filtered_roster(self):
        return {k: v for k, v in self.raw_roster().items() if k.endswith(self.gateway_domain)}

    def dry_msg(self):
        return '(DRYRUN) ' if self.dry_run else ''


def add_new_cards(update_xmpp: UpdateXMPP):
    for number, info in update_xmpp.cards_by_number.items():
        new_jid = f'{number}{update_xmpp.at_gw_domain()}'
        if new_jid not in update_xmpp.filtered_roster():
            log.info(f"{update_xmpp.dry_msg()}Adding {info['name']} ({number})")
            if not update_xmpp.dry_run:
                update_xmpp.roster_obj.Subscribe(new_jid)
                update_xmpp.roster_obj.Authorize(new_jid)
        else:
            log.debug(f"Contact {info['name']} ({number}) exists on XMPP Server")


def update_group_chat_names(update_xmpp: UpdateXMPP):
    cards_by_number = update_xmpp.cards_by_number
    for jid, data in update_xmpp.filtered_roster().items():
        if jid.endswith(update_xmpp.at_gw_domain()) and ',' in jid:
            numbers_without_domain = jid.rstrip(update_xmpp.at_gw_domain())
            group_numbers = numbers_without_domain.split(',')
            new_display_list = []
            for num in group_numbers:
                if num in cards_by_number:
                    new_display_list.append(cards_by_number[num]['name'])
                else:
                    new_display_list.append(num)
            group_display_name = ", ".join(new_display_list)
            log.debug(f"Found Group Chat {numbers_without_domain}")
            log.debug(f"Updated Chat Name {group_display_name}")

            clan = extract_clan(data['name']) if data['name'] else ""
            new_name = f"{clan}{group_display_name}"

            if new_name != data['name']:
                log.info(f"{update_xmpp.dry_msg()}Updating group chat {data['name']}")
                log.info(f"{update_xmpp.dry_msg()}Updated Name {new_name}")
                if not update_xmpp.dry_run:
                    update_xmpp.roster_obj.setItem(jid, name=new_name)
            else:
                log.debug(f"No Update for {new_name}")


def update_display_names(update_xmpp: UpdateXMPP):
    cards_by_number = update_xmpp.cards_by_number
    for jid, data in update_xmpp.filtered_roster().items():
        if jid.endswith(update_xmpp.at_gw_domain()) and ',' not in jid:
            phone = jid.rstrip(update_xmpp.at_gw_domain())
            if phone not in cards_by_number:
                log.info(f'{phone} Not Found in Address Book')
            else:
                cur_card = cards_by_number[phone]
                updated_name = cur_card['name']
                update_groups = new_groups(cur_card['groups'], data['groups'], update_xmpp.managed_groups)
                do_update = False
                if cards_by_number[phone]['name'] != data['name']:
                    log.info(f'{update_xmpp.dry_msg()}Updating {phone}. Setting name to {updated_name}')
                    do_update = True
                if not set(update_groups) == set(data['groups']):
                    log.info(f"{update_xmpp.dry_msg()}Updating groups for {cur_card['name']} to {update_groups}")
                    do_update = True
                if do_update and not update_xmpp.dry_run:
                    update_xmpp.roster_obj.setItem(jid, name=updated_name, groups=update_groups)


def ensure_dual_subscriptions(update_xmpp: UpdateXMPP):
    for num, data in update_xmpp.filtered_roster().items():
        if data['subscription'] != 'both':
            log.info(f"{update_xmpp.dry_msg()}Subscribing {num}")
            if not update_xmpp.dry_run:
                update_xmpp.roster_obj.Subscribe(num)
                update_xmpp.roster_obj.Authorize(num)


def xmpp_client(x_config):
    jid = xmpp.protocol.JID(x_config['jid'])
    con = xmpp.Client(server=jid.getDomain(), debug=[])
    con.connect()
    con.auth(jid.getNode(), x_config['password'])
    return con
