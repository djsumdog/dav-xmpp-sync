# TODO

[_] Fix Docker Image Pipeline
[_] Maintain XMPP connection in sync mode to handle incoming texts
[_] Find a way to address group chats not on roster
[_] Default group for all group chats

# DONE

[x] Support for multiperson SMS
[x] YAML schema validation
[x] Convert project to Poetry
[x] Create pip bin/main executable
[x] Update Docker to use poetry
[x] Add service/continual sync mode with interval